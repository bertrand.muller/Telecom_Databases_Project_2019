package main

import (
	"app/internal/app/indexing"
	"github.com/blevesearch/bleve"
	"fmt"
)

func main() {

	/*********************** INDEXATION ***********************/
	// indexXML := indexing.IndexDrugbankXML()
	indexXML, _ := bleve.Open("indexes/drugbank_xml.bleve")

	// Print the number of elements in the index
	fmt.Println(indexXML.DocCount())

	indexXML.Close()


	/*********************** SEARCH ***********************/
	hitsXML := indexing.SearchHitsInDrugbankXMLIndex("Siplizumab")

	hitXML := hitsXML[0]

	/* NAME */
	fmt.Println("--- NAME")
	name := indexing.GetValueFieldOfXMLHit(*hitXML, "Name")
	fmt.Println(name)

	/* DESCRIPTION */
	fmt.Println("--- DESCRIPTION")
	descriptionXML := indexing.GetValueFieldOfXMLHit(*hitXML, "Description")
	fmt.Println(descriptionXML)

	/* TOXICITY */
	fmt.Println("--- TOXICITY")
	toxicity := indexing.GetValueFieldOfXMLHit(*hitXML, "Toxicity")
	fmt.Println(toxicity)

	/* INDICATION */
	fmt.Println("--- INDICATION")
	indication := indexing.GetValueFieldOfXMLHit(*hitXML, "Indication")
	fmt.Println(indication)

}