package main

import (
	"app/internal/app/indexing"
	"github.com/blevesearch/bleve"
	"fmt"
	"os"
)

/*********** MAIN TO TEST OMIM INDEX ***********/
/*********** NEED TO BE REMOVED LATER ON ***********/
func main() {

	/*********************** INDEXATION ***********************/
	// Get OMIM_TXT index
	// index := indexing.IndexOmimTxt()
	index, _ := bleve.Open("indexes/omim_txt.bleve")

	// Print the number of elements in the index
	fmt.Println(index.DocCount())

	index.Close()


	/*********************** SEARCH ***********************/
	hits := indexing.SearchHitsInOmimIndex("TETRALOGY OF FALLOT AND GLAUCOMA")

	// List all the documents ("Hits") found in the index
	for i := 0; i < len(hits); i++ {
		fmt.Println(hits[i].Fields)
	}


	/*********************** USE RESULTS TO LOOK AT SOURCE FILE ***********************/

	// Test file.Seek method to check stored offsets
	file, _ := os.Open("data/omim.txt")
	defer file.Close()

	hit := hits[0]

	/* NAME */
	fmt.Println("--- NAME")
	ID := fmt.Sprintf("%f", hit.Fields["ID"].(float64))
	name := indexing.GetName(ID)
	fmt.Println(name)
	fmt.Println("")

	/* DESCRIPTION */
	fmt.Println("--- DESCRIPTION")
	description := indexing.GetValueFieldOfHit(*hit, "Description")
	fmt.Println(description)

	/* INHERITANCE */
	fmt.Println("--- INHERITANCE")
	inheritance := indexing.GetValueFieldOfHit(*hit, "Inheritance")
	fmt.Println(inheritance)

	/* PATHOGENESIS */
	fmt.Println("--- PATHOGENESIS")
	pathogenesis := indexing.GetValueFieldOfHit(*hit, "Pathogenesis")
	fmt.Println(pathogenesis)

	/* SYMPTOMS */
	fmt.Println("--- SYMPTOMS")
	symptoms := indexing.GetValueFieldOfHit(*hit, "Symptoms")
	fmt.Println(symptoms)

}



