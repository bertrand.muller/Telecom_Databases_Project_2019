package main

import (
	"app/internal/app/indexing"
	// "github.com/blevesearch/bleve"
	"fmt"
)

func main() {

	/*********************** INDEXATION ***********************/
	indexCSV := indexing.IndexOmimCSV()
	// indexCSV, _ := bleve.Open("indexes/omim_csv.bleve")

	// Print the number of elements in the index
	fmt.Println(indexCSV.DocCount())

	indexCSV.Close()

}