// Golang SwaggerUI example
//
// This documentation describes example APIs found under https://github.com/ribice/golang-swaggerui-example
//
//     Schemes: http
//     BasePath: /
//     Version: 1.0.0
//     Contact:
//     - Ophélien Amsler <ophelien.amsler@telecomnancy.eu>
//     - Pierrick Massin <pierrick.massin@telecomnancy.eu>
//     - Bertrand Muller <bertrand.muller@telecomnancy.eu>
//     - Teddy Valette <teddy.valette@telecomnancy.eu>
//
//     Consumes:
//     - application/multipartform
//     - application/urlform
//
//     Produces:
//     - application/json
//
// swagger:meta

package main

import (
	"app/internal/app/helpers"
	"app/internal/app/helpers/drivers"
	"app/internal/app/routes"
	"context"
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"time"
)

func main() {

	// Get configuration from the config file
	config := helpers.GetConfig()
	err := config.LoadFromFile("configs/config.development.json")
	if err != nil {
		panic(err)
	}

	// Create CouchDB connection
	configDB := config.DB.Couchdb
	couchClient := drivers.InitializeCouchClient(configDB.Username, configDB.Password, configDB.Host, configDB.Port, configDB.Schema)
	defer couchClient.Close(context.TODO())

	// Create SQL connection
	configSQL := config.DB.SQL
	sqlDriver := drivers.InitializeSQLClient(configSQL.Username, configSQL.Password, configSQL.Host, configSQL.Port, configSQL.Database)
	defer sqlDriver.DB.Close()

	// Create Gin Engine with the Logger and Recovery middleware already attached
	engine := gin.Default()

	engine.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  false,
		AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge:           time.Hour * 12,
	}))

	// Add web and api routes
	routes.Web(engine)
	routes.API(engine)

	// Listen and Server in 0.0.0.0:8888
	engine.Run(fmt.Sprintf(":%d", config.Port))
}
