# Télécom Nancy Databases 2019 Backend - _INSTALL_

- Clone the project 

  ```shell
  $ git clone git@gitlab.com:bertrand.muller3/Telecom_Databases_Project_2019.git
  ```

- Go to the backend folder and make backend/ your new GOPATH

  ```shell
  $ cd backend
  $ source env-setup.sh
  ```

 Note : use env-setup.sh if you are using bash/dash/... shell, use env-setup.fish if you are using fish shell

- Install govendor (dependance manager)

  ```shell
  $ go get -u github.com/kardianos/govendor
  ```

- Go to the vendor folder and install dependencies

  ```shell
  $ govendor sync
  ```

- Run the program

  ```shell
  $ go run cmd/main.go
  ```

- You can also run it through docker with docker-compose

  ```shell
  $ cd deployments
  $ sudo docker-compose --project-name telecom_databases_project_2019 up
  ```

- Import json files into CouchDB (for a fresh install only)
  ```shell
  $ cd /root/of/gmd/project
  $ cd /data
  $ curl -X PUT "http://forge:forge@127.0.0.1:5984/disease"
  $ curl -X PUT "http://forge:forge@127.0.0.1:5984/clinical_sign"
  $ curl -X PUT "http://forge:forge@127.0.0.1:5984/disease_clinical_sign"
  $ curl -X POST "http://forge:forge@127.0.0.1:5984/disease/_bulk_docs" -d @disease.json -H 'Content-Type:application/json'
  $ curl -X POST "http://forge:forge@127.0.0.1:5984/clinical_sign/_bulk_docs" -d @clinical_sign.json -H 'Content-Type:application/json'
  $ curl -X POST "http://forge:forge@127.0.0.1:5984/disease_clinical_sign/_bulk_docs" -d @disease_clinical_sign.json -H 'Content-Type:application/json'
  ```

  If you have errors like `chttpd_auth_cache changes listener died database_does_not_exist`, you can avoid them by going to this link [http://127.0.0.1:5984/_utils](http://127.0.0.1:5984/_utils). Then you have to click on "Setup" (wrench icon) and configure your setup for a single node.

- Import sql dump into MariaDB (for a fresh install only)
  ```shell
  $ cd /root/of/gmd/project
  $ cd /data
  $ mysql -u forge -pforge < neptune-dump.sql
  ```