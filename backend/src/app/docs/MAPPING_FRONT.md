# Télécom Nancy Databases 2019 Backend - _MAPPING_

Interroger la base disease.json => grâce au nom de la maladie cela permettra de récupérer plusieurs infos :

- Un lien vers le boulot d'un expert https://www.orpha.net/consor/cgi-bin/OC_Exp.php?lng=en&Expert=5 
- Les synonyms de la maladie (on peut ensuite rejouer une requête d'après les synonyms pour plus de résultat ? #pointbonus)
- le texte de la frequence (très souvent/très rare etc...)
- Les externals links qui vont nous servir à requête les autres bases

Ensuite on reprend l'id de disease et on le met dans disease-clinical signs pour récupérer la liste des clinicals signs à requêter (associé à cette maladie). Tout ça se passe via le Json ORPHA

Puis à l'aide de ces externals links on va aller jouer dans les autres bases :

Dans la base SQL (neptune) grâce au "{"id":"63897","Source":"MEDDRA","Reference":10048230}" du json :
dans la table meddra on va executer un select all where meddra_id = reference

"SELECT * FROM `meddra` WHERE `meddra_id` = 10048230"

Ca va nous renvoyer un concept type (inutile à ma connaissance ?)  et surtout un CUID !

Ce CUID va nous servir d'identifiant pour la suite de nos recherches dans la base SQL et dans les autres tables.

// !!!! \\\\ Autre méthode pour récupérer le CUID ! Parfois il existe un UMLS dans la base .json (cela correspond à un CUID).


Champs utiles dans la base sql : 
- Le nom des sides effects 
- la frequence (en pourcentage)

Puis on va aller dans omim.txt pour chercher d'après l'omim reference (qu'on a obtenu dans les external links)

Si cette id n'existe pas on peut aussi se servir de omim_onto.csv qui contient le lien CUID => OmimID 

Dans omim on trouvera une description (string) de la maladie ainsi que l'inehritance (hérédité) et le PATHOGENESIS (les effets de la maladie)


On a donc au final :

depuis disease.json : Le nom de la maladie + ses synonyms et la frequence (texte) + des liens vers les autres json + des liens génériques
depuis les autres json : Les signes cliniques (symptomes)

depuis les liens génériques : on va requêter sql pour récupérer  sides effects + frequence 

depuis omim on va récupérer le texte à afficher : Description / Hérédité / Pathogéne


Correction, le lien entre une maladie et un médicament ne s'effectue que sur drugbank (le xml).
Il faut donc faire une recherche dans drugbank pour obtenir toutes les informations d'un médicament depuis un nom de maladie (champ indication dans drugbank).

Nous pouvons extraire plusieurs informations de drugbank : la toxicité (effet secondaire du médicament), le nom, son dosage, la voie d'aministration et à quoi il sert en général.


On peut venir de deux fichiers, soit un .json (orpha) soit un .txt (omim). En effet les maladies présents dans un des fichiers ne sont pas forcément présent dans l'autre (mais peuvent être rare ET génétique).
Dans le cas orpha, on prend l'umls (==CUID) ou le meddra-id pour chercher le CUID dans la table meddra de la BDD SQL (neptune).
Dans le cas omim (.txt) on cherche la maladie et on va dans le fichier omim_onto.csv pour récupérer le CUID avant de poursuivre le chemin habituel.