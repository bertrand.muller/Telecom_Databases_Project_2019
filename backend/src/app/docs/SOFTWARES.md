# Télécom Nancy Databases 2019 Backend - _SOFTWARES_

- [Gin](https://github.com/gin-gonic/gin)

Gin is a HTTP web framework written in Go (Golang). It features a Martini-like API with much better performance -- up to 40 times faster. If you need smashing performance, get yourself some Gin

- [Kivik](https://github.com/go-kivik/kivik)

Kivik provides a common interface to CouchDB or CouchDB-like databases for Go and GopherJS. 

- [Bleve](https://github.com/blevesearch/bleve)

A modern text indexing library for go 