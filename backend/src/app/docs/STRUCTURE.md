# Télécom Nancy Databases 2019 Backend - _STRUCTURE_

- Schema of the structure :

  ```
  backend/
  ├── ...
  └── src
      └── app
          ├── api
          ├── cmd
          ├── configs
          ├── deployments
          ├── docs
          ├── examples
          ├── internal
          │   └── app
          │       ├── controllers
          │       └── routes
          └── vendor
  ```



- api/

OpenAPI/Swagger specs, JSON schema files, protocol definition files.

- cmd/

Main applications for this project.

- configs/

Configuration file templates or default configs.

- deployments/

IaaS, PaaS, system and container orchestration deployment configurations and templates (docker-compose, kubernetes/helm, mesos, terraform, bosh).

- examples/

Examples for your applications and/or public libraries.

- docs/

Design and user documents (in addition to your godoc generated documentation).

- internals/

Private application and library code. This is the code you don't want others importing in their applications or libraries.

- vendor/

Application dependencies (managed manually or by your favorite dependency management tool like dep).

**Source : [Go - Project Layout](https://github.com/golang-standards/project-layout)**