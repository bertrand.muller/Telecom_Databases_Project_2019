package controllers

// Controller is the generic controller that is used by others as a common parent
type Controller struct{}
