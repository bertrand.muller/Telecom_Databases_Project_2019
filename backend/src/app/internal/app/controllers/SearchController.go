package controllers

import (
	"app/internal/app/helpers/drivers"
	"app/internal/app/indexing"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"net/http"
	"reflect"
	"strconv"
	"strings"
)

// SearchController is a demo controller to show how we can use them
type SearchController struct {
	Controller
}

type MeddraRow struct {
	Cui      string
	MeddraID int
	Label    string
}

// SearchDiseaseByName method
// all information related to the focused disease
// @param c *gin.Context : the context from Gin
func (s SearchController) SearchDiseaseByName(c *gin.Context) {
	// Get disease name from URL
	diseaseName := c.Params.ByName("name")

	// Retrieve data from OrphaData (CouchDB)
	couchDataArray, externalReferencesIDs := retrieveDiseaseData(diseaseName)
	omimDataArray, _ := retrieveGeneticDiseaseData(diseaseName)

	// Retrieve data from Sider (SQL server)
	sqlDataArray := make([]interface{}, 0)

	//fmt.Println(externalReferencesIDs)
	// For each type of reference
	for key, value := range externalReferencesIDs {
		switch key {
		// If we process UMLS IDs
		case "UMLS":
			{
				// For each ID
				for _, umlsID := range value.([]string) {
					// We ask for the MySQL server information about this specific cui
					sqlDataArray = append(sqlDataArray, retrieveSQLData("cui", umlsID))
				}
			}
		// If we process MEDDRA IDs
		case "MEDDRA":
			{
				// For each ID
				for _, meddraID := range value.([]string) {
					// We ask for the MySQL server information about this specific meddra_id
					sqlDataArray = append(sqlDataArray, retrieveSQLData("meddra_id", meddraID))
				}
			}
		// If we process OMIM IDs
		case "OMIM":
			{
				// For each ID
				for _, omimID := range value.([]string) {
					// We ask for the information about this specific omimID in the index
					gDisease, _ := retrieveGeneticDiseaseData(omimID)
					for _, gDiseaseRow := range gDisease {
						omimDataArray = append(omimDataArray, gDiseaseRow)
					}
				}
			}
		default:
			// Nothing to do
		}
	}

	// Format the response to the frontend
	var dataArray = make(map[string][]interface{})
	dataArray["Orpha"] = couchDataArray
	dataArray["Omim"] = omimDataArray
	dataArray["Sider"] = sqlDataArray

	// Just some example to remove later
	//fmt.Println(dataArray["orpha"][0].(map[string]interface{})["ExternalReferenceList"].(map[string]interface{})["ExternalReference"].([]interface{})[4].(map[string]interface{})["Reference"])
	//fmt.Println(couchDataArray[0].(map[string]interface{})["ExternalReferenceList"].(map[string]interface{})["ExternalReference"].([]interface{})[4].(map[string]interface{})["Reference"])

	// Respond to the user
	c.JSON(http.StatusOK, gin.H{"data": dataArray})
}

// SearchDiseasesByCaracteristics method
// collection of diseases which are sharing the same caracterstics
// @param c *gin.Context : the context from Gin
func (s SearchController) SearchDiseasesByCaracteristics(c *gin.Context) {
	var inputData map[string]interface{}

	// Bind post data to the inputData variable
	err := c.ShouldBindBodyWith(&inputData, binding.JSON)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"data": err})
		panic(err)
	}

	// Execute the recursive function to process the request
	outputData := executeCaracteristicRequest(inputData)

	jsonResult := make(map[string]interface{})

	jsonResult["Orpha"] = outputData["disease_data"]
	jsonResult["ClinicalSigns"] = outputData["sign_data"]
	jsonResult["Omim"] = outputData["omim_data"]
	jsonResult["Sider"] = outputData["meddra_data"]
	jsonResult["Drugbank"] = outputData["drug_data"]

	// Send the result to the user
	c.JSON(http.StatusOK, gin.H{"data": jsonResult})
}

// executeCaracteristicRequest is a recursive function and return data matching with the content of input
// @param input map[string]interface{} : contains the query
// @return map[string][]interface{} : the result of the query
func executeCaracteristicRequest(input map[string]interface{}) map[string][]interface{} {

	var result = make(map[string][]interface{}, 0)

	// For each value in the query
	for key, value := range input {
		if strings.EqualFold(typeof(value), "map[string]interface {}") { // Value is Field - Leaf

			// We get the retrieve result of the leaf
			result = executeCaracteristicRequest(value.(map[string]interface{}))

		} else if strings.EqualFold(typeof(value), "[]interface {}") { // Value is Logic Operator

			// We get data from the two child (like a binary tree)
			resultLeft := executeCaracteristicRequest(value.([]interface{})[0].(map[string]interface{}))
			resultRight := executeCaracteristicRequest(value.([]interface{})[1].(map[string]interface{}))

			// Check what data we get from the childs
			_, diseaseExistsInLeft := resultLeft["disease_data"]
			_, diseaseExistsInRight := resultRight["disease_data"]

			_, omimExistsInLeft := resultLeft["omim_data"]
			_, omimExistsInRight := resultRight["omim_data"]

			_, meddraExistsInLeft := resultLeft["meddra_data"]
			_, meddraExistsInRight := resultRight["meddra_data"]

			_, signExistsInLeft := resultLeft["sign_data"]
			_, signExistsInRight := resultRight["sign_data"]

			_, drugExistsInLeft := resultLeft["drug_data"]
			_, drugExistsInRight := resultRight["drug_data"]

			if strings.EqualFold(key, "AND") { // Logic Operator is AND

				if diseaseExistsInLeft && diseaseExistsInRight { // If we have disease data in both children results
					tmpSlice := make([]interface{}, 0)

					// For each disease from the left child
					for _, rowLeft := range resultLeft["disease_data"] {
						diseaseIDLeft := rowLeft.(map[string]interface{})["id"].(string)

						// For each disease from the right child
						for _, rowRight := range resultRight["disease_data"] {
							diseaseIDRight := rowRight.(map[string]interface{})["id"].(string)

							// If we find the same disease ID, then we store it
							if strings.EqualFold(diseaseIDLeft, diseaseIDRight) {
								tmpSlice = append(tmpSlice, rowRight)
							}
						}
					}
					result["disease_data"] = append(result["disease_data"], tmpSlice)
				}
				//else if diseaseExistsInLeft && !diseaseExistsInRight { // If we have disease data in left child result
				//	result["disease_data"] = append(result["disease_data"], resultLeft["disease_data"])
				//} else if !diseaseExistsInLeft && diseaseExistsInRight { // If we have disease data in right child result
				//	result["disease_data"] = append(result["disease_data"], resultRight["disease_data"])
				//}

				if omimExistsInLeft && omimExistsInRight { // If we have omim data in both children results
					tmpSlice := make([]interface{}, 0)

					// For each omim from the left child
					for _, rowLeft := range resultLeft["omim_data"] {
						omimNameLeft := rowLeft.(map[string]interface{})["name"].(string)

						// For each omim from the right child
						for _, rowRight := range resultRight["omim_data"] {
							omimNameRight := rowRight.(map[string]interface{})["name"].(string)

							// If we find the same omim name, then we store it
							if strings.EqualFold(omimNameLeft, omimNameRight) {
								tmpSlice = append(tmpSlice, rowRight)
							}
						}
					}
					result["omim_data"] = append(result["omim_data"], tmpSlice)
				}
				//else if omimExistsInLeft && !omimExistsInRight { // If we have omim data in left child result
				//	result["omim_data"] = append(result["omim_data"], resultLeft["omim_data"])
				//} else if !omimExistsInLeft && omimExistsInRight { // If we have omim data in right child result
				//	result["omim_data"] = append(result["omim_data"], resultRight["omim_data"])
				//}

				if meddraExistsInLeft && meddraExistsInRight { // If we have meddra data in both children results
					tmpSlice := make([]interface{}, 0)

					// For each omim from the left child
					for _, rowLeft := range resultLeft["meddra_data"] {
						meddraIDLeft := rowLeft.([]interface{})[0].(MeddraRow).MeddraID

						// For each omim from the right child
						for _, rowRight := range resultRight["meddra_data"] {
							meddraIDRight := rowRight.([]interface{})[0].(MeddraRow).MeddraID

							// If we find the same omim name, then we store it
							if meddraIDLeft == meddraIDRight {
								tmpSlice = append(tmpSlice, rowRight)
							}
						}
					}
					result["meddra_data"] = append(result["meddra_data"], tmpSlice)
				}
				//else if meddraExistsInLeft && !meddraExistsInRight { // If we have meddra data in left child result
				//	result["meddra_data"] = append(result["meddra_data"], resultLeft["meddra_data"])
				//} else if !meddraExistsInLeft && meddraExistsInRight { // If we have meddra data in right child result
				//	result["meddra_data"] = append(result["meddra_data"], resultRight["meddra_data"])
				//}

				if signExistsInLeft && signExistsInRight { // If we have sign data in both children results
					tmpSlice := make([]interface{}, 0)

					// For each sign from the left child
					for _, rowLeft := range resultLeft["sign_data"] {
						diseaseIDLeft := rowLeft.(map[string]interface{})["clinicalSign"].(map[string]interface{})["id"].(string)

						// For each sign from the right child
						for _, rowRight := range resultRight["sign_data"] {
							diseaseIDRight := rowRight.(map[string]interface{})["clinicalSign"].(map[string]interface{})["id"].(string)

							// If we find the same sign ID, then we store it
							if strings.EqualFold(diseaseIDLeft, diseaseIDRight) {
								tmpSlice = append(tmpSlice, rowRight)
							}
						}
					}
					result["sign_data"] = append(result["sign_data"], tmpSlice)
				}
				//else if signExistsInLeft && !signExistsInRight { // If we have sign data in left child result
				//	result["sign_data"] = append(result["sign_data"], resultLeft["sign_data"])
				//} else if !signExistsInLeft && signExistsInRight { // If we have sign data in right child result
				//	result["sign_data"] = append(result["sign_data"], resultRight["sign_data"])
				//}

				if drugExistsInLeft && drugExistsInRight { // If we have drug data in both children results
					tmpSlice := make([]interface{}, 0)

					// For each sign from the left child
					for _, rowLeft := range resultLeft["drug_data"] {
						fmt.Println(rowLeft)
						drigIDLeft := rowLeft.([]interface{})[0].(map[string]interface{})["name"].(string)

						// For each sign from the right child
						for _, rowRight := range resultRight["drug_data"] {
							drigIDRight := rowRight.([]interface{})[0].(map[string]interface{})["name"].(string)

							// If we find the same sign ID, then we store it
							if strings.EqualFold(drigIDLeft, drigIDRight) {
								tmpSlice = append(tmpSlice, rowRight)
							}
						}
					}
					result["sign_data"] = append(result["sign_data"], tmpSlice)
				}
				//else if drugExistsInLeft && !drugExistsInRight { // If we have drug data in left child result
				//	result["drug_data"] = append(result["drug_data"], resultLeft["drug_data"])
				//} else if !drugExistsInLeft && drugExistsInRight { // If we have drug data in right child result
				//	result["drug_data"] = append(result["drug_data"], resultRight["drug_data"])
				//}

			} else if strings.EqualFold(key, "OR") { // Logic Operator is AND

				if diseaseExistsInLeft && diseaseExistsInRight { // If we have disease data in both children results
					result["disease_data"] = append(result["disease_data"], resultLeft["disease_data"])
					result["disease_data"] = append(result["disease_data"], resultRight["disease_data"])
				} else if diseaseExistsInLeft && !diseaseExistsInRight { // If we have disease data in left child result
					result["disease_data"] = append(result["disease_data"], resultLeft["disease_data"])
				} else if !diseaseExistsInLeft && diseaseExistsInRight { // If we have disease data in right child result
					result["disease_data"] = append(result["disease_data"], resultRight["disease_data"])
				}

				if omimExistsInLeft && omimExistsInRight { // If we have omim data in both children results
					result["omim_data"] = append(result["omim_data"], resultLeft["omim_data"])
					result["omim_data"] = append(result["omim_data"], resultRight["omim_data"])
				} else if omimExistsInLeft && !omimExistsInRight { // If we have omim data in left child result
					result["omim_data"] = append(result["omim_data"], resultLeft["omim_data"])
				} else if !omimExistsInLeft && omimExistsInRight { // If we have omim data in right child result
					result["omim_data"] = append(result["omim_data"], resultRight["omim_data"])
				}

				if meddraExistsInLeft && meddraExistsInRight { // If we have meddra data in both children results
					result["meddra_data"] = append(result["meddra_data"], resultLeft["meddra_data"])
					result["meddra_data"] = append(result["meddra_data"], resultRight["meddra_data"])
				} else if meddraExistsInLeft && !meddraExistsInRight { // If we have meddra data in left child result
					result["meddra_data"] = append(result["meddra_data"], resultLeft["meddra_data"])
				} else if !meddraExistsInLeft && meddraExistsInRight { // If we have meddra data in right child result
					result["meddra_data"] = append(result["meddra_data"], resultRight["meddra_data"])
				}

				if signExistsInLeft && signExistsInRight { // If we have sign data in both children results
					result["sign_data"] = append(result["sign_data"], resultLeft["sign_data"])
					result["sign_data"] = append(result["sign_data"], resultRight["sign_data"])
				} else if signExistsInLeft && !signExistsInRight { // If we have sign data in left child result
					result["sign_data"] = append(result["sign_data"], resultLeft["sign_data"])
				} else if !signExistsInLeft && signExistsInRight { // If we have sign data in right child result
					result["sign_data"] = append(result["sign_data"], resultRight["sign_data"])
				}

				if drugExistsInLeft && drugExistsInRight { // If we have drug data in both children results
					result["drug_data"] = append(result["drug_data"], resultLeft["drug_data"])
					result["drug_data"] = append(result["drug_data"], resultRight["drug_data"])
				} else if drugExistsInLeft && !drugExistsInRight { // If we have drug data in left child result
					result["drug_data"] = append(result["drug_data"], resultLeft["drug_data"])
				} else if !drugExistsInLeft && drugExistsInRight { // If we have drug data in right child result
					result["drug_data"] = append(result["drug_data"], resultRight["drug_data"])
				}

			}

		} else { // Value is Leaf

			if strings.EqualFold(key, "name") { // Leaf is Name

				// We get data related to the given disease name
				tmpResult, externalReferencesIDs := retrieveDiseaseData(value.(string))
				if len(tmpResult) > 0 {
					result["disease_data"] = tmpResult
				}

				for key, value := range externalReferencesIDs {
					switch key {
					// If we process UMLS IDs
					case "UMLS":
						{
							// For each ID
							for _, umlsID := range value.([]string) {
								// We ask for the MySQL server information about this specific cui
								ulmsResult := retrieveSQLData("cui", umlsID)
								if len(ulmsResult) > 0 {
									result["meddra_data"] = append(result["meddra_data"], ulmsResult)
								}
							}
						}
					// If we process MEDDRA IDs
					case "MEDDRA":
						{
							// For each ID
							for _, meddraID := range value.([]string) {
								// We ask for the MySQL server information about this specific meddra_id
								meddraResult := retrieveSQLData("meddra_id", meddraID)
								if len(meddraResult) > 0 {
									result["meddra_data"] = append(result["meddra_data"], meddraResult)
								}
							}
						}
					// If we process OMIM IDs
					case "OMIM":
						{
							// For each ID
							for _, omimID := range value.([]string) {
								// We ask for the information about this specific omimID in the index
								gDisease, _ := retrieveGeneticDiseaseData(omimID)
								for _, gDiseaseRow := range gDisease {
									if len(tmpResult) > 0 {
										result["omim_data"] = append(result["omim_data"], gDiseaseRow)
									}
								}
							}
						}
					default:
						// Nothing to do
					}
				}

				// Retrieve disease from OMIM (genetic diseases)
				tmpResult, _ = retrieveGeneticDiseaseData(value.(string))
				if len(tmpResult) > 0 {
					result["omim_data"] = append(result["omim_data"], tmpResult)
				}

			} else if strings.EqualFold(key, "signandorsymptoms") { // Leaf is Sign/Symptom

				// We get data related to the given sign/symptom name
				tmpResult := retrieveClinicalSignsData(value.(string))
				if len(tmpResult) > 0 {
					result["sign_data"] = tmpResult
				}

				// Retrieve symptom from OMIM (genetic diseases)
				tmpResult, _ = retrieveGeneticDiseaseData(value.(string))
				if len(tmpResult) > 0 {
					result["omim_data"] = append(result["omim_data"], tmpResult)
				}

			} else if strings.EqualFold(key, "drug") { // Leaf is drug

				// Searching for drugs related to this disease
				tmpResult := retrieveDrugData(value.(string))
				if len(tmpResult) > 0 {
					result["drug_data"] = append(result["drug_data"], tmpResult)
				}
			}

		}
	}

	return result

}

// typeof return the type of an interface
// @param v interface{} : the interface we cant to analyze
// @return string : the type of the given interface
func typeof(v interface{}) string {
	return reflect.TypeOf(v).String()
}

// retrieveClinicalSignsData is used to process CouchDB datas
// @param signName string : the name of the sign we are searching for
// @return []interface{} : an array containing all rows
func retrieveClinicalSignsData(signName string) []interface{} {
	var cSignDataArray []interface{}

	// Create connection to clinical_sign database
	couchCSignDB := drivers.CreateCouchDatabaseInterface("clinical_sign")
	defer couchCSignDB.DB.Close(context.TODO())

	// Query for disease database
	// We are asking for all documents which contains the disease's name in the Name field or in the list of synonym
	var couchCSignQuery = `
	{
		"selector": {
			"value.clinicalSign.Name.text": {
				"$regex": "` + fmt.Sprintf("(?i)%s", signName) + `"
			}
		}
	}`

	// Execute the query to get all document from disease matching with our query
	cSignRows, err := couchCSignDB.DB.Find(context.TODO(), couchCSignQuery)
	if err != nil {
		panic(err)
	}
	defer cSignRows.Close()

	// For each results
	for cSignRows.Next() {
		// Inject the result into a variable
		var cSignRow map[string]interface{}
		err = cSignRows.ScanDoc(&cSignRow)
		if err != nil {
			panic(err)
		}

		// Add final disease row into the final array
		cSignDataArray = append(cSignDataArray, cSignRow["value"])
	}

	return cSignDataArray
}

// retrieveDiseaseData is used to process CouchDB datas
// @param diseaseName string : the name of the disease we are searching for
// @return []interface{} : an array containing all rows
// @return map[string]interface{} : amap with all external references' IDs
func retrieveDiseaseData(diseaseName string) ([]interface{}, map[string]interface{}) {
	// ---- COUCHDB -----
	var couchDataArray []interface{}
	externalReferencesIDs := make(map[string]interface{})

	// Create connection to disease database
	couchDiseaseDB := drivers.CreateCouchDatabaseInterface("disease")
	defer couchDiseaseDB.DB.Close(context.TODO())

	// Create connection to disease_clinical_sign database
	couchDiseaseClinicalSignDB := drivers.CreateCouchDatabaseInterface("disease_clinical_sign")
	defer couchDiseaseDB.DB.Close(context.TODO())

	// Query for disease database
	// We are asking for all documents which contains the disease's name in the Name field or in the list of synonym
	var couchDiseaseQuery = `
	{
		"selector": {
			"$or": [
				{
					"value.Name.text": {
						"$regex": "` + fmt.Sprintf("(?i)%s", diseaseName) + `"
					}
				},
				{
					"value.SynonymList.Synonym": {
						"$elemMatch": {
							"text": {
								"$regex": "` + fmt.Sprintf("(?i)%s", diseaseName) + `"
							}
						}
					}
				}
			]
		},
		"fields": [
			"value.ExpertLink.text", "value.ExternalReferenceList", "value.Name.text", "value.OrphaNumber", "value.SynonymList", "value.id"
		]
	}`

	// Execute the query to get all document from disease matching with our query
	diseaseRows, err := couchDiseaseDB.DB.Find(context.TODO(), couchDiseaseQuery)
	if err != nil {
		panic(err)
	}
	defer diseaseRows.Close()

	// Prepare slices which will stock external references IDs
	umlsIDArray := make([]string, 0)
	meddraIDArray := make([]string, 0)
	omimIDArray := make([]string, 0)

	// For each results
	for diseaseRows.Next() {
		// Inject the result into a variable
		var diseaseRow map[string]interface{}
		err = diseaseRows.ScanDoc(&diseaseRow)
		if err != nil {
			panic(err)
		}

		// Get all external references
		var diseaseExternalReferenceList = diseaseRow["value"].(map[string]interface{})["ExternalReferenceList"]
		var diseaseExternalReferenceCount = diseaseExternalReferenceList.(map[string]interface{})["count"].(string)

		// Manage the case where there is no external references
		intCount, err := strconv.Atoi(diseaseExternalReferenceCount)
		if intCount > 0 {
			var diseaseExternalReference []interface{}

			// Manage the case where ExternalReference isn't a slice
			if intCount == 1 {
				diseaseExternalReference = append(diseaseExternalReference, diseaseExternalReferenceList.(map[string]interface{})["ExternalReference"].(interface{}))
			} else {
				diseaseExternalReference = diseaseExternalReferenceList.(map[string]interface{})["ExternalReference"].([]interface{})
			}

			// For each external references
			for _, reference := range diseaseExternalReference {
				mappedReference := reference.(map[string]interface{})

				// Sorting IDs
				if strings.EqualFold(mappedReference["Source"].(string), "UMLS") {
					umlsIDArray = append(umlsIDArray, mappedReference["Reference"].(string))
				} else if strings.EqualFold(mappedReference["Source"].(string), "MEDDRA") {
					meddraIDArray = append(meddraIDArray, fmt.Sprintf("%.0f", mappedReference["Reference"]))
				} else if strings.EqualFold(mappedReference["Source"].(string), "OMIM") {
					omimIDArray = append(omimIDArray, fmt.Sprintf("%.0f", mappedReference["Reference"]))
				}
			}

			// Format the final external references IDs map
			externalReferencesIDs["UMLS"] = umlsIDArray
			externalReferencesIDs["MEDDRA"] = meddraIDArray
			externalReferencesIDs["OMIM"] = omimIDArray
		}

		// Retrieve from the result its ID and add it into a new query to get all related clinical signs
		var diseaseID = diseaseRow["value"].(map[string]interface{})["id"]
		var couchDiseaseClinicalSignQuery = `
		{
			"selector": {
			   "value.disease.id": "` + fmt.Sprintf("%s", diseaseID) + `"
			},
			"fields": [
				"value.clinicalSign", "value.data"
			]
		}
		`

		// Execute the query to get all document from disease_clinical_sign matching with our last query
		diseaseClinicalSignRows, err := couchDiseaseClinicalSignDB.DB.Find(context.TODO(), couchDiseaseClinicalSignQuery)
		if err != nil {
			panic(err)
		}
		defer diseaseClinicalSignRows.Close()

		// For each result
		var diseaseClinicalSignArray []interface{}
		for diseaseClinicalSignRows.Next() {
			// Inject the result into a variable
			var diseaseClinicalSignRow map[string]interface{}
			err = diseaseClinicalSignRows.ScanDoc(&diseaseClinicalSignRow)
			if err != nil {
				panic(err)
			}

			// Remove _id and _rev from our result
			diseaseClinicalSignArray = append(diseaseClinicalSignArray, diseaseClinicalSignRow["value"])
		}
		// Adding clinical sings into the disease row
		diseaseRow["value"].(map[string]interface{})["clinical_signs"] = diseaseClinicalSignArray

		// Searching for drugs related to this disease
		realDiseaseName := diseaseRow["value"].(map[string]interface{})["Name"].(map[string]interface{})["text"].(string)
		drugData := retrieveDrugData(realDiseaseName)
		drugMap := make(map[string][]interface{})
		for _, drugRow := range drugData {

			// If this drug is on indication section, then we flag it as "indication"
			if caseInsenstiveContains(drugRow.(map[string]interface{})["indication"].(string), realDiseaseName) {
				drugMap["indication"] = append(drugMap["indication"], drugRow)
			}

			// If this drug is on toxicity section, then we flag it as "toxicity"
			if caseInsenstiveContains(drugRow.(map[string]interface{})["toxicity"].(string), realDiseaseName) {
				drugMap["toxicity"] = append(drugMap["toxicity"], drugRow)
			}
		}
		diseaseRow["value"].(map[string]interface{})["drugs"] = drugMap

		// Add final disease row into the final array
		couchDataArray = append(couchDataArray, diseaseRow["value"])
	}

	return couchDataArray, externalReferencesIDs
	// ---- END COUCHDB -----
}

func caseInsenstiveContains(a, b string) bool {
	return strings.Contains(strings.ToUpper(a), strings.ToUpper(b))
}

// retrieveDiseaseData is used to process CouchDB datas
// @param field string : the field of the disease we are searching for
// @return []interface{} : an array containing all rows
// @return map[string]interface{} : amap with all external references' IDs
func retrieveGeneticDiseaseData(field string) ([]interface{}, map[string]interface{}) {
	dataArray := make([]interface{}, 0)
	externalReferencesIDs := make(map[string]interface{})

	/*********************** SEARCH ***********************/
	hits := indexing.SearchHitsInOmimIndex(field) //"TETRALOGY OF FALLOT AND GLAUCOMA"

	/*********************** USE RESULTS TO LOOK AT SOURCE FILE ***********************/

	for _, hit := range hits {
		hitMap := make(map[string]interface{})

		idHit := fmt.Sprintf("%.0f", hit.Fields["ID"].(float64))
		hitMap["id"] = idHit

		name := indexing.GetName(idHit)
		hitMap["name"] = name

		/* DESCRIPTION */
		description := indexing.GetValueFieldOfHit(*hit, "Description")
		hitMap["description"] = description

		/* INHERITANCE */
		inheritance := indexing.GetValueFieldOfHit(*hit, "Inheritance")
		hitMap["inheritance"] = inheritance

		/* PATHOGENESIS */
		pathogenesis := indexing.GetValueFieldOfHit(*hit, "Pathogenesis")
		hitMap["pathogenesis"] = pathogenesis

		/* SYMPTOMS */
		symptoms := indexing.GetValueFieldOfHit(*hit, "Symptoms")
		hitMap["symptoms"] = symptoms

		dataArray = append(dataArray, hitMap)
	}

	return dataArray, externalReferencesIDs
}

func retrieveDrugData(field string) []interface{} {
	dataArray := make([]interface{}, 0)

	/*********************** SEARCH ***********************/
	hits := indexing.SearchHitsInDrugbankXMLIndex(field)

	/*********************** USE RESULTS TO LOOK AT SOURCE FILE ***********************/

	for _, hit := range hits {
		hitMap := make(map[string]interface{})

		/* ID */
		id := indexing.GetValueFieldOfXMLHit(*hit, "ID")
		hitMap["id"] = id

		/* NAME */
		name := indexing.GetValueFieldOfXMLHit(*hit, "Name")
		hitMap["name"] = name

		/* DESCRIPTION */
		description := indexing.GetValueFieldOfXMLHit(*hit, "Description")
		hitMap["description"] = description

		/* TOXICITY */
		toxicity := indexing.GetValueFieldOfXMLHit(*hit, "Toxicity")
		hitMap["toxicity"] = toxicity

		/* INDICATION */
		indication := indexing.GetValueFieldOfXMLHit(*hit, "Indication")
		hitMap["indication"] = indication

		dataArray = append(dataArray, hitMap)
	}

	return dataArray
}

// retrieveSQLData is used to process SQL datas
// @param field string : the field which will filter the request (for instance : cui or meddra_id)
// @param filter string : the value of the filter (used with the field parameter)
// @return []interface{} : an array containing all rows
func retrieveSQLData(field string, filter string) []interface{} {
	// Create or get SQL connection
	sqlDriver := drivers.GetSQLClient()

	// Create the prepared statement
	var sqlQuery = `SELECT DISTINCT(cui), meddra_id, label FROM meddra WHERE ` + field + ` = "` + filter + `";`

	// Execute the query
	sqlRows, err := sqlDriver.DB.Query(sqlQuery)
	if err != nil {
		panic(err)
	}
	defer sqlRows.Close()

	// Delcaring variable to process result of the query
	var sqlDataArray []interface{}

	// For each result
	for sqlRows.Next() {
		// Inject result into a variable
		var row MeddraRow
		err = sqlRows.Scan(&row.Cui, &row.MeddraID, &row.Label)
		if err != nil {
			panic(err)
		}

		// Add the result into the final array
		sqlDataArray = append(sqlDataArray, row)
	}

	return sqlDataArray
}
