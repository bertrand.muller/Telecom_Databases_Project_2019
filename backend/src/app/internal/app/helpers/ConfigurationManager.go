package helpers

import (
	"encoding/json"
	"os"
)

// SQLConfig contains SQL credentials and database
type SQLConfig struct {
	Username string
	Password string
	Host     string
	Port     string
	Database string
}

// CouchConfig contains CouchDB credentials and URL
type CouchConfig struct {
	Username string
	Password string
	Host     string
	Port     string
	Schema   string
}

// DBConfig will contain all databases configurations
type DBConfig struct {
	Couchdb CouchConfig
	SQL     SQLConfig
}

// Configuration is the type containing all data related to the current configuration
type Configuration struct {
	Port int
	DB   DBConfig
}

// LoadFromFile method
// @bindTo c Configuration
// @param path string : the path of the file which will be loaded into c
// @return error : return an error if something went wrong
func (c *Configuration) LoadFromFile(path string) error {
	//filename is the path to the json config file
	file, err := os.Open(path)
	if err != nil {
		return err
	}

	decoder := json.NewDecoder(file)

	err = decoder.Decode(c)
	if err != nil {
		return err
	}

	return nil
}

// Configuration variable
// Instance of Configuration
var config = Configuration{}

// GetConfig method
// @return Configuration an instance of Configuration
func GetConfig() Configuration {
	return config
}
