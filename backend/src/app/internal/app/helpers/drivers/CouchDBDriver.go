package drivers

import (
	"context"
	"fmt"
	_ "github.com/go-kivik/couchdb" // Used by kivik as driver
	"github.com/go-kivik/kivik"
)

// CouchDBDriver contains the interface with CouchDB
type CouchDBDriver struct {
	Driver
	DB *kivik.DB
}

// static variable (sort of singleton)
var client *kivik.Client

// InitializeCouchClient initialize the connection with CouchDB
// @param username string : the username we use to connect to CouchDB
// @param password string : the password we use to connect to CouchDB
// @param url string : the url of CouchDB
// @param port string : the port which is used by CouchDB (should be 5984 with a fresh install)
// @param schema string : the http schema (can be http or https)
// @return *kivik.Client : connection object
func InitializeCouchClient(username string, password string, url string, port string, schema string) *kivik.Client {
	var err error
	// Create connection with CouchDB
	client, err = kivik.New("couch", fmt.Sprintf("%s://%s:%s@%s:%s", schema, username, password, url, port))
	if err != nil {
		panic(err)
	}

	return client
}

// CreateCouchDatabaseInterface create a DB object
// @param dbname string : the database name we want to work with
// @return CouchDBDriver : the object containing the DB connection
func CreateCouchDatabaseInterface(dbname string) CouchDBDriver {
	// We check if CouchDB is reacheable
	result, err := client.Ping(context.TODO())
	if err != nil {
		panic(err)
	}

	if result {
		// If CouchDB is reacheable, we check if the database exists
		result, err := client.DBExists(context.TODO(), dbname)
		if err != nil {
			panic(err)
		}

		if result {
			// If the database exists, we return the DB connection object
			return CouchDBDriver{DB: client.DB(context.TODO(), dbname)}
		}
		panic(fmt.Sprintf("Database %s not found !", dbname))

	} else {
		panic("CouchDB Client isn't reachable ! Make sure you have initialized the client and the server is online.")
	}
}
