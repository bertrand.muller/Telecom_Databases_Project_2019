package drivers

// Driver is the generic Driver that is used by others as a common parent
type Driver struct{}
