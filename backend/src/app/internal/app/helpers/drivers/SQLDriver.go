package drivers

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql" // Used by sql as driver
)

// SQLDriver contains the DB connection object
type SQLDriver struct {
	Driver
	DB *sql.DB
}

// Static variable
var sqldriver SQLDriver

// InitializeSQLClient initialize the connection with MariaDB
// @param username string : the username we use to connect to MariaDB
// @param password string : the password we use to connect to MariaDB
// @param host string : the host of MariaDB
// @param port string : the port which is used by MariaDB (should be 3306 with a fresh install)
// @param database string : the database name
// @return SQLDriver : the DB connection object
func InitializeSQLClient(username string, password string, host string, port string, database string) SQLDriver {
	// We connect to the MariaDB server
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, host, port, database))
	if err != nil {
		panic(err)
	}

	// We check if the server is reacheable
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	// We return the connection object
	sqldriver = SQLDriver{DB: db}
	return sqldriver
}

// GetSQLClient return the connection object
// @return SQLDriver : the DB connection object
func GetSQLClient() SQLDriver {
	return sqldriver
}
