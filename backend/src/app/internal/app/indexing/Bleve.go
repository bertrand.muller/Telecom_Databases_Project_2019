package indexing

import (
	"github.com/blevesearch/bleve"
	"log"
)

var index bleve.Index

// Function to get the corresponding index
func getBleveIndex(indexName string) bleve.Index {
	index, err := GetIndex(indexName)
	if err != nil {
		log.Fatalln("Error while getting index !")
	}
	return index
}

// Get index and a po
func GetIndex(indexName string) (bleve.Index, error) {

	// If omimIndex isn't set
	if index == nil {
		var err error

		// Try to open persistence file
		index, err = bleve.Open(indexName)

		// If file does not exist or something goes wrong
		if err != nil {

			// Create a new mapping file and create a new index
			indexMapping := bleve.NewIndexMapping()
			index, err = bleve.New(indexName, indexMapping)

			if err != nil {
				return nil, err
			}
		}
	}

	// Return the omimIndex (previously or newly created)
	return index, nil
}