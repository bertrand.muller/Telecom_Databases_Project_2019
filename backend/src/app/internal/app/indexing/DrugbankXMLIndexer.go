package indexing

import (
	"encoding/xml"
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/search"
	"io/ioutil"
	"log"
	"os"
	"fmt"
)

// Define a structure for a drug
type Drug struct {
	XMLName xml.Name `xml:"drug"`
	ID string `xml:"drugbank-id"`
	Name string `xml:"name"`
	Description string `xml:"description"`
	Toxicity string `xml:"toxicity"`
	Indication string `xml:"indication"`
}

type Drugbank struct {
	XMLName xml.Name `xml:"drugbank"`
	Drugs []Drug `xml:"drug"`
}

type DrugIndex struct {
	ID string
	Name string
	Description string
	Toxicity string
	Indication string
}


// Index Drugbank XML file
func IndexDrugbankXML() bleve.Index {

	// Setup
	indexName := "indexes/drugbank_xml.bleve"
	index := getBleveIndex(indexName)

	// Index file completely
	indexFileXML(index)

	// Return created index
	return index

}


// Function used to index the complete file
func indexFileXML(index bleve.Index) {

	// Loop over all drugbank subsets
	for i := 1 ; i <= 6 ; i++ {

		// Open corresponding file to create the wanted index
		file, err := os.Open(fmt.Sprintf("../../../data/drugbank_%d.xml", i))
		fmt.Println(fmt.Sprintf("../../../data/drugbank_%d.xml", i))
		if err != nil {
			fmt.Println(fmt.Sprintf("Error while opening 'drugbank_%d' file !", i))
		}
		defer file.Close()
	
		// Map all objects with defined structs
		var drugbank Drugbank
		XMLdata, _ := ioutil.ReadAll(file)
		xml.Unmarshal(XMLdata, &drugbank)
	
		// Loop over all drugs and index each of them
		drugs := drugbank.Drugs
		for i:= 0; i < len(drugs); i++ {
			id, drugToIndex := getDrugDataFromFile(drugs[i])
			fmt.Println(id)
			indexXMLData(id, drugToIndex, index)
		}
	
	}
	
}


// Search for drug data in XML file
func getDrugDataFromFile(drug Drug) (string, DrugIndex) {

	drugToIndex := DrugIndex {
		ID:          drug.ID,
		Name:        drug.Name,
		Description: drug.Description,
		Toxicity:    drug.Toxicity,
		Indication:  drug.Indication,
	}

	return drugToIndex.ID, drugToIndex

}


// Function used to index a document
func indexXMLData(ID string, document DrugIndex, index bleve.Index) {
	err := index.Index(ID, document)
	if err != nil {
		log.Fatalln("Error while indexing XML data !")
	}
}


// Function used to search in the index (according to a string)
func SearchHitsInDrugbankXMLIndex(match string) search.DocumentMatchCollection {

	index, _ := bleve.Open("indexes/drugbank_xml.bleve")

	// Create a query
	query := bleve.NewQueryStringQuery(match)

	// Create a search request to retrieve documents matching the query
	searchRequest := bleve.NewSearchRequest(query)
	searchRequest.Fields = []string{
		"ID",
		"Name",
		"Description",
		"Toxicity",
		"Indication",
	}
	searchResults, err := index.Search(searchRequest)

	// Verify that no error has been raised, while searching for documents
	if err != nil {
		log.Fatalln("Error while searching for documents matching the string !")
	}

	index.Close()

	return searchResults.Hits

}


/* Function used to get field value of a hit
*  Fields available :
*		- Name
*		- Description
*		- Toxicity
*		- Indication
 */
func GetValueFieldOfXMLHit(hit search.DocumentMatch, field string) interface{} {

	index, _ := bleve.Open("indexes/drugbank_xml.bleve")
	fieldValue := hit.Fields[field]

	index.Close()

	return fieldValue
}