package indexing

import (
	"github.com/blevesearch/bleve"
)

type Indexer interface {
	IndexFile(index bleve.Index)
	IndexData(ID int, document interface{}, index bleve.Index)
}