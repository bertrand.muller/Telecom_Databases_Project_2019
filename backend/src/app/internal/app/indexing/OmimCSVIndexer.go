package indexing

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/search"
	"io"
	"log"
	"os"
)

// Define s structure for a record
type Record struct {
	ID string
	Name string
	Synonyms string
	CUI string
}


// Index OMIM CSV file
func IndexOmimCSV() bleve.Index {

	// Setup
	indexName := "indexes/omim_csv.bleve"
	index := getBleveIndex(indexName)

	// Index file completely
	indexOmimCSVFile(index)

	// Return created index
	return index

}


// Function used to index the complete file
func indexOmimCSVFile(index bleve.Index) {

	for i := 1 ; i <= 35 ; i++ {

		// Open corresponding file to create the wanted index
		file, err := os.Open(fmt.Sprintf("../../../data/omim_onto_%d.csv", i))
		fmt.Println(fmt.Sprintf("../../../data/omim_onto_%d.csv", i))
		if err != nil {
			log.Println(fmt.Sprintf("Error while opening 'omim_onto_%d' file !", i))
		}
		defer file.Close()

		// Create reader
		scanner := csv.NewReader(bufio.NewReader(file))

		i := 0
		for {
			line, error := scanner.Read()
			if error == io.EOF {
				break;
			} else if error != nil {
				log.Fatal(error)
			}
			i++
			if(i % 50 == 0){
				fmt.Println(i)
			}
			data := Record {
				ID: line[0],
				Name: line[1],
				Synonyms: line[2],
				CUI: line[5],
			}
			indexCSVData(data, index)
		}

	}
	

}


// Function used to index a record instance
func indexCSVData(document Record, index bleve.Index) {
	err := index.Index(document.ID, document)
	if err != nil {
		log.Fatalln("Error while indexing data !")
	}
}


// Function used to search in the index (according to a string)
func SearchHitsInOmimCSVIndex(match string) search.DocumentMatchCollection {

	index, _ := bleve.Open("indexes/omim_csv.bleve")

	// Create a query
	query := bleve.NewQueryStringQuery(match)

	// Create a search request to retrieve documents matching the query
	searchRequest := bleve.NewSearchRequest(query)
	searchRequest.Fields = []string{
		"ID",
		"Name",
		"Synonyms",
		"CUI",
	}
	searchResults, err := index.Search(searchRequest)

	// Verify that no error has been raised, while searching for documents
	if err != nil {
		log.Fatalln("Error while searching for documents matching the string !")
	}

	index.Close()

	return searchResults.Hits

}