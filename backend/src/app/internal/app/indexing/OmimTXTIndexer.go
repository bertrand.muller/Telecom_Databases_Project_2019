package indexing

import (
	"bufio"
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/search"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

// Define a structure for a sickness
type Sickness struct {
	ID                      int
	Name                    string
	DescriptionStartOffset  int64
	DescriptionEndOffset    int64
	InheritanceStartOffset  int64
	InheritanceEndOffset    int64
	PathogenesisStartOffset int64
	PathogenesisEndOffset   int64
	SymptomsStartOffset     int64
	SymptomsEndOffset       int64
}

// Index OMIM text file
func IndexOmimTxt() bleve.Index {

	// Setup
	indexName := "indexes/omim_txt.bleve"
	index := getBleveIndex(indexName)

	// Index file completely
	indexFile(index)

	// Return created index
	return index

}

// Function used to index the complete file
func indexFile(index bleve.Index) {

	// Open corresponding file to create the wanted index
	file, err := os.Open("../../../data/omim.txt")
	if err != nil {
		log.Println("Error while opening the file 'data/omim.txt' !")
	}

	// Create reader
	scanner := bufio.NewReader(file)

	// Offset from start
	offset := int64(0)

	// Loop over the file to read each line
	line := ""
	for i := 0; i < 4193820; i++ {
		line = getNextLine(scanner, &offset)
		if strings.HasPrefix(line, "*RECORD*") {
			id, data := getSicknessDataFromFile(scanner, &offset)
			indexData(id, data, index)
		} else if strings.HasPrefix(line, "*THEEND*") {
			break
		}
	}
}

// Search for sickness data in OMIM file
func getSicknessDataFromFile(scanner *bufio.Reader, offset *int64) (int, Sickness) {

	// Set some default values for the current sickness to index
	id := -1
	name := "no_name"
	descriptionStart := int64(-1)
	descriptionEnd := int64(-1)
	inheritanceStart := int64(-1)
	inheritanceEnd := int64(-1)
	pathogenesisStart := int64(-1)
	pathogenesisEnd := int64(-1)
	symptomsStart := int64(-1)
	symptomsEnd := int64(-1)

	// Variables
	line := ""

	// Loop over the file until next record is detected
	for !strings.HasPrefix(line, "*RECORD*") && !strings.HasPrefix(line, "*THEEND*") {

		// Check if it is "ID" section
		if strings.HasPrefix(line, "*FIELD* NO") {
			line = getNextLine(scanner, offset)
			regex := regexp.MustCompile(`\r?\n`)
			idTmp := regex.ReplaceAllString(line, "")
			id, _ = strconv.Atoi(idTmp)

			// Check if it is "NAME" section
		} else if strings.HasPrefix(line, "*FIELD* TI") {
			line = getNextLine(scanner, offset)
			name = line

			// Check if it is "TEXT" section
		} else if strings.HasPrefix(line, "*FIELD* TX") {
			line = getNextLine(scanner, offset)

			startDescription := false
			startInheritance := false
			startPathogenesis := false

			stopDescription := false
			stopInheritance := false
			stopPathogenesis := false

			for !stopDescription || !stopInheritance || !stopPathogenesis {

				// Wait for "DESCRIPTION"
				if strings.HasPrefix(line, "DESCRIPTION") {
					descriptionStart = *offset
					startDescription = true
				}

				// Wait for "INHERITANCE"
				if strings.HasPrefix(line, "INHERITANCE") && !strings.HasPrefix(line, "INHERITANCE:") {
					inheritanceStart = *offset
					startInheritance = true
				}

				// Wait for "PATHOGENESIS"
				if strings.HasPrefix(line, "PATHOGENESIS") {
					pathogenesisStart = *offset
					startPathogenesis = true
				}

				// Check if we start with the next section
				match, _ := regexp.MatchString(`^((\r\n|\r|\n){1})*([A-Z]+\s*[A-Z]*)((\r\n|\r|\n){1})*$`, line)
				if match {

					// Add end offset for "DESCRIPTION"
					if startDescription && !strings.HasPrefix(line, "DESCRIPTION") {
						descriptionEnd = *offset - int64(len([]byte(line)))
						startDescription = false
						stopDescription = true
					}

					// Add end offset for "INHERITANCE"
					if startInheritance && !strings.HasPrefix(line, "INHERITANCE") {
						inheritanceEnd = *offset - int64(len([]byte(line)))
						startInheritance = false
						stopInheritance = true
					}

					// Add end offset for "PATHOGENESIS"
					if startPathogenesis && !strings.HasPrefix(line, "PATHOGENESIS") {
						pathogenesisEnd = *offset - int64(len([]byte(line)))
						startPathogenesis = false
						stopPathogenesis = true
					}

				} else if strings.HasPrefix(line, "*FIELD*") {
					stopDescription = true
					stopInheritance = true
					stopPathogenesis = true

					// "DESCRIPTION"
					if startDescription {
						descriptionEnd = *offset - int64(len([]byte(line)))
					}

					// "INHERITANCE"
					if startInheritance {
						inheritanceEnd = *offset - int64(len([]byte(line)))
					}

					// "PATHOGENESIS"
					if startPathogenesis {
						pathogenesisEnd = *offset - int64(len([]byte(line)))
					}
				}

				line = getNextLine(scanner, offset)

			}

			// Check if it is "SYMPTOMS" section
		} else if strings.HasPrefix(line, "*FIELD* CS") {
			symptomsStart = *offset
			line = getNextLine(scanner, offset)
			for !strings.HasPrefix(line, "*") {
				line = getNextLine(scanner, offset)
			}

			// Remove offset for new section
			symptomsEnd = *offset - int64(len([]byte(line)))
		}

		// Get next line
		line = getNextLine(scanner, offset)
	}

	// Create a sickness instance for current record
	sickness := Sickness{
		ID:                      id,
		Name:                    name,
		DescriptionStartOffset:  descriptionStart,
		DescriptionEndOffset:    descriptionEnd,
		InheritanceStartOffset:  inheritanceStart,
		InheritanceEndOffset:    inheritanceEnd,
		PathogenesisStartOffset: pathogenesisStart,
		PathogenesisEndOffset:   pathogenesisEnd,
		SymptomsStartOffset:     symptomsStart,
		SymptomsEndOffset:       symptomsEnd,
	}

	return sickness.ID, sickness
}

// Function used to index a sickness instance
func indexData(ID int, document Sickness, index bleve.Index) {
	err := index.Index(strconv.Itoa(ID), document)
	if err != nil {
		log.Fatalln("Error while indexing TXT data !")
	}
}

// Function used to get the name of a sickness according to an ID
func GetName(ID string) string {

	index, _ := bleve.Open("indexes/omim_txt.bleve")

	// Create a query
	query := bleve.NewQueryStringQuery(ID)

	// Create a search request to the name matching the ID
	searchRequest := bleve.NewSearchRequest(query)
	searchRequest.Fields = []string{
		"Name",
	}
	searchResults, err := index.Search(searchRequest)

	// Verify that no error has been raised, while searching for Name
	if err != nil {
		log.Fatalln("Error while searching for documents matching the string !")
	}

	index.Close()

	name := searchResults.Hits[0].Fields["Name"].(string)
	positionSpace := strings.Index(name, " ")
	positionComma := strings.Index(name, ";")

	if positionSpace != -1 {
		positionSpace++
	} else {
		positionSpace = 0
	}

	if positionComma == -1 {
		positionComma = len(name)
	}

	name = name[positionSpace:positionComma]

	return name

}

// Function used to search in the index (according to a string)
func SearchHitsInOmimIndex(match string) search.DocumentMatchCollection {

	index, _ := bleve.Open("indexes/omim_txt.bleve")

	// Create a query
	query := bleve.NewQueryStringQuery(match)

	// Create a search request to retrieve documents matching the query
	searchRequest := bleve.NewSearchRequest(query)
	searchRequest.Fields = []string{
		"ID",
		"Name",
		"DescriptionStartOffset",
		"DescriptionEndOffset",
		"InheritanceStartOffset",
		"InheritanceEndOffset",
		"PathogenesisStartOffset",
		"PathogenesisEndOffset",
		"SymptomsStartOffset",
		"SymptomsEndOffset",
	}
	searchResults, err := index.Search(searchRequest)

	// Verify that no error has been raised, while searching for documents
	if err != nil {
		log.Fatalln("Error while searching for documents matching the string !")
	}

	index.Close()

	return searchResults.Hits
}

/* Function used to get field value of a hit
*  Fields available :
*		- Description
*		- Inheritance
*		- Pathogenesis
*		- Symptoms
 */
func GetValueFieldOfHit(hit search.DocumentMatch, field string) string {

	// Open OMIM file
	file, _ := os.Open("../../../data/omim.txt")
	defer file.Close()

	// Offset start
	offsetStartF := hit.Fields[field+"StartOffset"].(float64)
	offsetStart := int64(offsetStartF)

	// Offset end
	offsetEndF := hit.Fields[field+"EndOffset"].(float64)
	offsetEnd := int64(offsetEndF)

	// Create a variable to store description
	// Size => offsetEnd - offsetStart
	difference := offsetEnd - offsetStart
	description := make([]byte, difference)

	// Read text
	_, _ = file.ReadAt(description, offsetStart)

	return string(description)
}
