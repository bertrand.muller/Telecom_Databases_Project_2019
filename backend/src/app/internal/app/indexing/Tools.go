package indexing

import "bufio"

// Function used to get the next line of a file & to increment offset
func getNextLine(scanner *bufio.Reader, offset *int64) string {
	line, _ := scanner.ReadString('\n')
	*offset += int64(len([]byte(line)))
	return string(line)
}
