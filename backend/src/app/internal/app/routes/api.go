package routes

import (
	"app/internal/app/controllers"
	"github.com/gin-gonic/gin"
)

var searchController = controllers.SearchController{}

// API Method
// Router which contains all API routes
// @param engine *gin.Engine : the Gin engine
func API(engine *gin.Engine) {

	// swagger:operation GET /search/atom/disease/{name} search atom disease GetAtomDisease
	// ---
	// summary: Retrieve all information related to the disease.
	// description: If no result, Error Not Found (404) will be returned.
	// parameters:
	// - name: name
	//   in: path
	//   description: name of the disease
	//   type: string
	//   required: true
	engine.GET("/search/atom/disease/:name", func(c *gin.Context) {
		searchController.SearchDiseaseByName(c)
	})

	// swagger:route POST /search/collection/diseases search collection diseases GetCollectionDiseases
	// Filtered collection of diseases
	// Returns a collection of diseases which are sharing the same caracterstics
	engine.POST("/search/collection/diseases", func(c *gin.Context) {
		searchController.SearchDiseasesByCaracteristics(c)
	})

}
