# Télécom Nancy Databases 2019 - _Commands_

- Executer un fichier .go sans compilation

  ```shell
  $ go run /path/to/file.go
  ```

- Compiler un fichier .go

  ```shell
  $ go build /path/to/file.go
  ```

