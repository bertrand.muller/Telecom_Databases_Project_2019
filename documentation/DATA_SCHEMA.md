# Télécom Nancy Databases 2019 - _Data Schema_

## clinical_signs `JSON`
- *id*
- *key*
- **value**
    - **clinicalSign**
        - *id*
        - **Name**
            - *lang*
            - *text*
    - **disease**
        - *id*
        - *OrphaNumber*
        - **Name**
            - **lang*
            - **text*
    - **data**
        - **signFreq**
            - *id*
            - **Name**
                - *lang*
                - *text*
                

**Exemple :** 
```JSON
{
    "id" : "en_product4.json",
    "key" : 2000,
    "value" : {
        "clinicalSign" : {
            "id" : "2000",
            "Name" : {
                "lang" : "en",
                "text" : "Skull/cranial anomalies"
            }
        },
        "disease" : {
            "id" : "1707",
            "OrphaNumber" : 1555,
            "Name" : {
                "lang" : "en",
                "text" : "Cutis gyrata - acanthosis nigricans - craniosynostosis"
            }
        },
        "data" : {
            "signFreq" : { 
                "id" : "640",
                "Name" : {
                    "lang" : "en",
                    "text" : "Very frequent"
                }
            }
        }
    }
 }
```

 **Raison d'être :**
 Donne toutes les maladies à partir d'un signe clinique

## disease `JSON`
- *id*
- *key*
- **value**
    - *id*
    - *OrphaNumber*
    - **ExpertLink**
        - *lang*
        - *text*
    - **Name**
        - *lang*
        - *text*
    - **DisorderFlagList**
        - *count*
        - ***DisorderFlag***
            - *id*
    - **SynonymList**
        - *count*
        - ***Synonym***
            - *lang*
            - *text*
    - **ExternalReferenceList**
        - *count*
        - ***ExternalReference***
            - *id*
            - *Source*
            - *Reference*
              

**Exemple :** 
```JSON
{
    "id" : "en_product1.json",
    "key" : 14,
    "value" : {
        "id" : "252",
        "OrphaNumber" : 14,
        "ExpertLink" : {
            "lang" : "en",
            "text" : "http://www.orpha.net/consor/cgi-bin/OC_Exp.php?lng=en&Expert=14"
        },
        "Name" : {
            "lang" : "en",
            "text" : "Abetalipoproteinemia"
        },
        "DisorderFlagList" : {
            "count" : "1",
            "DisorderFlag" : {
                "id" : "2"
            }
        },
        "SynonymList" : {
            "count" : "2",
            "Synonym" : [
                {
                    "lang" : "en",
                    "text" : "Bassen-Kornzweig disease"
                },
                {
                    "lang" : "en",
                    "text" : "Homozygous familial hypobetalipoproteinemia"
                }
            ]
        },
        "ExternalReferenceList" : {
            "count" : "5",
            "ExternalReference" : [
                {
                    "id" : "4111",
                    "Source" : "OMIM",
                    "Reference" : 200100
                },
                {
                    "id" : "62488",
                    "Source" : "MESH",
                    "Reference" : "D000012"
                },
                {
                    "id" : "4112",
                    "Source" : "ICD10",
                    "Reference" : "E78.6"
                },
                {
                    "id" : "62487",
                    "Source" : "SNOMED CT",
                    "Reference" : 190787008
                },
                {
                    "id" : "62486",
                    "Source" : "UMLS",
                    "Reference" : "C0000744"
                }
            ]
        }
    }
}
```

**Raison d'être :**
Donne les détails d'une maladie

## disease_clinical_signs `JSON`
- *id*
- *key*
- **value**
    - **disease**
        - *id*
        - *OrphaNumber*
        - **Name**
            - *lang*
            - *text*
    - **clinicalSign**
        - *id*
        - **Name**
            - *lang*
            - *text*
    - **data***
        - **signFreq**
            - *id*
            - **Name**
                - *lang*
                - *text*
                

**Exemple :** 
```JSON
{
    "id" : "en_product4.json",
    "key" : 6,
    "value" : {
        "disease" : {
            "id" : "3297",
            "OrphaNumber" : 6,
            "Name" : {
                "lang" : "en",
                "text" : "Isolated 3-methylcrotonyl-CoA carboxylase deficiency"
            }
        },
        "clinicalSign" : {
            "id" : "49340",
            "Name" : {
                "lang" : "en",
                "text" : "Organic acid metabolism anomalies"
            }
        },
        "data" : {
            "signFreq" : {
                "id" : "640",
                "Name" : {
                    "lang" : "en",
                    "text" : "Very frequent"
                }
            }
        }
    }
}
```

 **Raison d'être :**
 Donne tous les signes cliniques à partir d'une maladie

## drugbank_full_database `XML`



## onim  `TXT`

<table>
    <thead>
    	<tr>
        	<th>Field</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><i>*RECORD*</i></td>
            <td>
                <table>
                    <thead>
                        <th>Field</th>
                        <th>Description</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td><i>*FIELD* NO</i></td>
                            <td>OMIM Number</td>
                    	</tr>
                    	<tr>
                            <td><i>*FIELD* TI</i></td>
                           	<td>Title</td>
                    	</tr>
                    	<tr>
                            <td><i>*FIELD* TX</i></td>
                            <td>Text</td>
                    	</tr>
                        <tr>
                            <td><i>*FIELD* CN</i></td>
                            <td>Contributors</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* CD</i></td>
                            <td>Creation Date</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* ED</i></td>
                            <td>Edit History</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* SA</i></td>
                            <td>See Also</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* RF</i></td>
                            <td>References</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* CS</i></td>
                            <td>Symptoms</td>
                        </tr>
                        <tr>
                            <td><i>*FIELD* AV</i></td>
                            <td>Allelic Variant List</td>
                        </tr>
                	</tbody>
            	</table>
            </td>
        </tr>
    </tbody>
</table>


**TXT Example :**

```TXT
*RECORD*
*FIELD* NO
100050

*FIELD* TI
100050 AARSKOG SYNDROME, AUTOSOMAL DOMINANT

*FIELD* TX
DESCRIPTION
Aarskog syndrome is characterized by short stature and facial...

CLINICAL FEATURES
Grier et al. (1983) reported father and 2 sons with...

INHERITANCE
Grier et al. (1983) tabulated the findings in 82 previously reported...

PATHOGENESIS
Toriello et al. (1988) suggested that the vascular changes in the skin...

MAPPING
Southgate et al. (2011) performed a genomewide screen in 22 members of 2...

MOLECULAR GENETICS
In 2 families with congenital scalp defects and distal limb reduction...

*FIELD* CN
Nara Sobreira - updated: 4/22/2013

*FIELD* CD
Victor A. McKusick: 6/4/1986

*FIELD* ED
carol: 04/24/2013

*FIELD* SA
Burton and Dillard (1984); Harley et al. (1972); Lee  (1977); Monie
and Monie (1979); Riccardi and Grum (1977); Roberts  (1956); Welch
and Kearney (1974); Woodhouse et al. (1982)

*FIELD* RF
1. Grier, R. E.; Farrington, F. H.; Kendig, R.; Mamunes, P.: Autosomal dominant inheritance of the Aarskog syndrome. Am. J. Med. Genet. 15: 39-46, 1983.

*FIELD* CS
Growth:
   Mild to moderate short stature

```



## omim_onto `CSV`

Column separator : `,`

<table>
    <thead>
    	<tr>
        	<th>Column</th>
            <th>Example</th>
            <th>Type</th>
        </tr>
    </thead>
    <tbody>
    	<tr>
            <td><i>Class ID</i></td>
        	<td>http://purl.bioontology.org/ontology/OMIM/600374</td>
            <td>String</td>
        </tr>
        <tr>
        	<td><i>Preferred Label</i></td>
            <td>BBS4 GENE</td>
            <td>String</td>
        </tr>
        <tr>
        	<td><i>Synonyms</i></td>
            <td>BBS4</td>
            <td>String</td>
        </tr>
        <tr>
        	<td><i>Definitions</i></td>
            <td>Blabla</td>
            <td>String</td>
        </tr>
        <tr>
        	<td><i>Obsolete</i></td>
            <td>false</td>
            <td>Boolean</td>
        </tr>
        <tr>
        	<td><i>CUI</i></td>
            <td>C1412749</td>
            <td>String</td>
        </tr>
        <tr>
        	<td><i>Semantic Types</i></td>
       	<td>http://purl.bioontology.org/ontology/STY/T028</td>
        	<td>String</td>
        </tr>
        <tr>
        	<td><i>Parents</i></td>
            <td>http://purl.bioontology.org/ontology/OMIM/MTHU000067</td>
            <td>String</td>
        </tr>
    </tbody>
</table>


**CSV Line Example :** 

```CSV
http://purl.bioontology.org/ontology/OMIM/600374,
BBS4 GENE,
BBS4,
Blabla,
false,
C1412749,
http://purl.bioontology.org/ontology/STY/T028,
http://purl.bioontology.org/ontology/OMIM/MTHU000067
```

## ORPHA_Mappings `OBO`
| Name | Quantity | Description |
| ----------- | ----------- | ----------- |
| id | 1 | Link to oprha.net |
| name | 1 | Disease name |
| xref | 0..* | Mapping to other data source |

**OBO Term example _(line)_**
```
[Term]
id: http://www.orpha.net/signs/rdfns#Sign00aa69c1-678f-4be1-9c8a-f5825f1c8bbb
name: Ovary malignant tumor
xref: ORPHA 50420 "Ovary/Fallopian tube neoplasm/tumor/carcinoma/cancer (excl. teratoma/germinoma)"
xref: PhenoDB: 2490 "Ovarian cancer"
xref: UMLS: C0029925 "Ovarian Carcinoma"
xref: UMLS: C1140680 "Malignant neoplasm of ovary"
xref: SNOMEDCT: 363443007 "Malignant tumor of ovary (disorder)"
xref: MESH: D010051 "Ovarian Neoplasms"
xref: MEDRA: 10033131 "Ovarian carcinoma"
xref: MEDRA: 10033128 "Ovarian cancer"
```
