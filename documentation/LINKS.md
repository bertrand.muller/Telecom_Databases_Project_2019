# Télécom Nancy Databases 2019 - _Links_

### JSON Parsing

[Tutorial - Soham Kamani - 2017](https://www.sohamkamani.com/blog/2017/10/18/parsing-json-in-golang/)

[Example - GoByExample](https://gobyexample.com/json)



### JSON Encode

[Tutorial - GoLang](https://golang.org/pkg/encoding/json/)



### XML Parsing

[Tutorial - Tutorial Edge - 2017](https://tutorialedge.net/golang/parsing-xml-with-golang/)

[Example - KeiruaProd - 2017](https://blog.keiruaprod.fr/2017/02/21/parsing-xml-in-go/)



### CSV Parsing

[Tutorial - The Polyglot Developer - 2017](https://www.thepolyglotdeveloper.com/2017/03/parse-csv-data-go-programming-language/)

[Example - DotNetPerls](https://www.dotnetperls.com/csv-go)



### Mediator Design Pattern
[Tutorial - DZone](https://dzone.com/articles/design-patterns-mediator)

[Tutorial - Sourcemaking](https://sourcemaking.com/design_patterns/mediator)

[Example DB - DesignPatterPHP](https://designpatternsphp.readthedocs.io/en/latest/Behavioral/Mediator/README.html)


### Query in go for SQL DataBase
[Tutorial] http://go-database-sql.org/overview.html

### OMIM

[API](http://omim.org/help/api)



### Multi-threading

[Reading file - Stackoverflow](https://stackoverflow.com/questions/27217428/reading-a-file-concurrently-in-golang)

[Multi-threaded writing to a CSV file - Mark Needham](https://markhneedham.com/blog/2017/01/31/go-multi-threaded-writing-csv-file/)

[Asynchronously Split an io.Reader in Go - Rodaine](https://rodaine.com/2015/04/async-split-io-reader-in-golang/)



### Indexation (Bleve for Go)

[Example - Indraniel](https://gist.github.com/indraniel/8108bd7def9b5e222417)

