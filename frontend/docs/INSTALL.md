# Télécom Nancy Databases 2019 Frontend - _INSTALL_

- Install npm

  ```shell
  $ apt install npm
  ```
- Download dependencies

  ```shell
  $ npm install
  $ npm install axios
  ```

- Launch the app

  ```shell
  $ npm run server -- --port 8889
  ```

- Open a web browser on

  ```http request
  http://127.0.0.1:8889
  ```